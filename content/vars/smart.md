+++
title = "Be Smart"
description = "SMART goes beyond colorful charts and exotic KPIs. It uses real-world connections to deliver insight into your local market ERP systems and staffing needs. It also helps you engage customers and solve problems before they know they have them. That's SMART."
weight = 0
render = true
template = "page.html"
[extra]
headerImg = "smart.jpg"
headerImgAlt = "Image of a networked brain"
headerHeadline = "smart thinking."

+++

<div class="features">

## SMART VARs
  * Drive a compelling narrative
  * Enjoy deeper customer relationships
  * Are more satisfied
  * Have competitive advantages
</div>

<div class="features">

## SMART Candidates
* Recognized for their expertise
* Sought after by companies
* Paid better 
* Happier and more successful
</div>

<div class="features">

## SMART Customers
* Begging for a new staffing story & methodology
* Looking to squeeze value out of every dollar spent.
* Pleading for measurable tools and insight
* Tired of the same old story. Does “We meet and check references on everyone we represent.” sound familiar to you?
</div>

<div class="features">

## SMART Businesses
* Slippery: They’re easy to slide into 
* Sticky: Once you get in, it’s hard to get out
* Proven: Proven ROI
* Scalable: To earn more and do less.
* Attractive: to investors: 
* SMART grows revenue--not headcount--with a scalable differentiator that delivers proven ROI for minimal risk.
</div>

